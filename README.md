# OpenML dataset: brazilian_houses

https://www.openml.org/d/44800

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

This dataset contains 10962 houses to rent in Brazil in 2020 with 13 diferent features.

The data was obtained as a result of web scraping and this is why there could be some values in the dataset that can be considered outliers.

**Attribute Description**

*rent_amount*, *property_tax*, *fire_insurance* columns should be ignored if *total* is taken as the target feature for the task, as they are part of *total*.

1. *city*
2. *area*
3. *rooms*
4. *bathroom*
5. *parking_spaces*
6. *floor*
7. *animal* - "accept" or "not accept"
8. *furniture* - "furnished" or "not furnished"
9. *hoa*
10. *rent_amount*
11. *property_tax*
12. *fire_insurance*
13. *total* - target feature

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44800) of an [OpenML dataset](https://www.openml.org/d/44800). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44800/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44800/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44800/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

